/**
 *
 * Инициализация галп'а
 *
 *     npm install gulp
 *     npm install --save-dev gulp
 *     npm install --save gulp gulp-browserify   gulp-concat gulp-styl gulp-livereload   tiny-lr  gulp-uglify  gulp-minify-css
 *
 **/

var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var styl = require('gulp-styl');
var refresh = require('gulp-livereload');
var lr = require('tiny-lr');
var server = lr();
var uglify = require("gulp-uglify");
var minifyCSS = require('gulp-minify-css');
var destFolder = "../src/main/webapp/static/yesrts/min";
var sourceFolderChange = '../src/main/webapp/static/yesrts/**';
var css_list = [];
var js_list = [];

/**
 *
 * Создаем файлы для галп
 *
 */
var layout_css = [
    "../src/main/webapp/static/yesrts/css/main.css",
    "../src/main/webapp/static/yesrts/css/styles.css",
    "../src/main/webapp/static/yesrts/css/custom.css",
    "../src/main/webapp/static/yesrts/css/jquery-ui.min.css",
    "../src/main/webapp/static/yesrts/css/jquery-ui.structure.min.css",
    "../src/main/webapp/static/yesrts/css/jquery-ui.theme.min.css"
];
css_list.push({source: layout_css, taskName: 'style_layout_css', fileName: 'layout.css'});
gulp.task('style_layout_css', function () {
    gulp.src(layout_css)
        // .pipe(styl({compress : true}))
        .pipe(minifyCSS({keepBreaks: false}))
        .pipe(concat('layout.css'))
        .pipe(gulp.dest(destFolder))
});

var layout_js = [
    "../src/main/webapp/static/topgrp/js/libraries/jquery.js",
    "../src/main/webapp/static/yesrts/js/vendor/jquery.min-432ffe11.js",
    "../src/main/webapp/static/yesrts/js/vendor/bootstrap.min-9056f769.js",
    "../src/main/webapp/static/yesrts/js/vendor/calculator.js",
    "../src/main/webapp/static/yesrts/js/layout.js",
    "../src/main/webapp/static/yesrts/js/vendor/jquery-ui.min.js"
];
js_list.push({source: layout_js, taskName: 'script_layout_js', fileName: 'layout.js'});
gulp.task('script_layout_js', function () {
    gulp.src(layout_js)
        //.pipe(browserify())
        .pipe(uglify())
        .pipe(concat('layout.js'))
        .pipe(gulp.dest(destFolder))
});
/*****************************************************************************************/


gulp.task('default', function () {
    for (var css_key in css_list) {
        gulp.run(css_list[css_key].taskName);
    }
    for (var js_key in js_list) {
        gulp.run(js_list[js_key].taskName);
    }

    gulp.watch([sourceFolderChange, '!' + destFolder + "/**"], function (event) {
        for (var css_key in css_list) {
            gulp.run(css_list[css_key].taskName);
        }
        for (var js_key in js_list) {
            gulp.run(js_list[js_key].taskName);
        }
    });

})