package com.ub.yesrts.blog.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.seo.seoTags.models.SeoTags;
import com.ub.ubblog.blog.models.BlogDoc;
import com.ub.ubblog.blog.services.BlogService;
import com.ub.yesrts.blog.routes.BlogRoutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class BlogsController {

    @Autowired private BlogService blogService;

    @RequestMapping(value = BlogRoutes.ALL, method = RequestMethod.GET)
    public String all(Model model) {
        List<BlogDoc> blogDocList = blogService.findActiveAll();
        SeoTags seoTags = new SeoTags();
        seoTags.setTitle("Шекспир | Новости");
        seoTags.setMetaTitle("Новости");
        seoTags.setMetaDescription("");
        seoTags.setMetaKeywords("");
        seoTags.setH1("Новости");

        model.addAttribute("blogDocList", blogDocList);
        model.addAttribute("seoTags", seoTags);
        return "com.ub.yesrts.client.blog.all";
    }

    @RequestMapping(value = BlogRoutes.VIEW, method = RequestMethod.GET)
    public String view(@PathVariable String url, Model model){

        BlogDoc blogDoc = blogService.findByUrl(url);
        if(blogDoc == null){
            return RouteUtils.redirectTo404();
        }
        model.addAttribute("blogDoc",blogDoc);
        model.addAttribute("seoTags", blogDoc.getSeoTags());
        return "com.ub.yesrts.client.blog.view";
    }
}
