package com.ub.yesrts.blog.routes;

public class BlogRoutes {
    private static final String ROOT = "/blog";
    public static final String ALL = ROOT + "/all";
    public static final String VIEW =  "/{url}";
}
