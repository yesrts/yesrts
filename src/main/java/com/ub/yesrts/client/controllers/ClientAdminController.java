package com.ub.yesrts.client.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.services.PictureService;
import com.ub.yesrts.client.models.ClientDoc;
import com.ub.yesrts.client.routes.ClientAdminRoutes;
import com.ub.yesrts.client.services.ClientService;
import com.ub.yesrts.client.views.all.SearchClientAdminRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClientAdminController {
    @Autowired private ClientService clientService;
    @Autowired private PictureService pictureService;


    @RequestMapping(value = ClientAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        ClientDoc clientDoc = new ClientDoc();
        clientDoc.setId(new ObjectId());
        clientDoc.setPicture(new ObjectId());
        model.addAttribute("clientDoc", clientDoc);
        return "com.ub.yesrts.admin.client.add" ;
    }

    @RequestMapping(value = ClientAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute ClientDoc clientDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        ObjectId id = pictureService.saveWithDelete(pic,null);
        clientDoc.setPicture(id);
        clientService.save(clientDoc);
        redirectAttributes.addAttribute("id",clientDoc.getId());
        return RouteUtils.redirectTo(ClientAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        ClientDoc clientDoc = clientService.findById(id);
        if (clientDoc.getPicture() == null) clientDoc.setPicture(new ObjectId());
        model.addAttribute("clientDoc", clientDoc);
        return "com.ub.yesrts.admin.client.edit" ;
    }

    @RequestMapping(value = ClientAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute ClientDoc clientDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        ClientDoc old = clientService.findById(clientDoc.getId());
        ObjectId id = pictureService.saveWithDelete(pic, old.getPicture());
        if(id!=null){
            clientDoc.setPicture(id);
        }
        clientService.save(clientDoc);
        redirectAttributes.addAttribute("id",clientDoc.getId());
        return RouteUtils.redirectTo(ClientAdminRoutes.EDIT);
    }

     @RequestMapping(value = ClientAdminRoutes.ALL, method = RequestMethod.GET)
     public String all( @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                           @RequestParam(required = false, defaultValue = "") String query,
                           Model model) {
            SearchClientAdminRequest searchClientAdminRequest = new SearchClientAdminRequest(currentPage);
            searchClientAdminRequest.setQuery(query);
            model.addAttribute("searchClientAdminResponse", clientService.findAll(searchClientAdminRequest));
            return "com.ub.yesrts.admin.client.all";
     }

    @RequestMapping(value = ClientAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
          return "com.ub.yesrts.admin.client.delete" ;
    }

    @RequestMapping(value = ClientAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        clientService.remove(id);
        return RouteUtils.redirectTo(ClientAdminRoutes.ALL);
    }
}
