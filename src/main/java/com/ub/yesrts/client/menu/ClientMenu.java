package com.ub.yesrts.client.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class ClientMenu extends CoreMenu{
    public ClientMenu() {
        this.name = "Клиенты";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
