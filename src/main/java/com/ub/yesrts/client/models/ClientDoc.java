package com.ub.yesrts.client.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
public class ClientDoc {
    @Id
    private ObjectId id;
    private String name;
    private String url;
    private Integer position = 1;
    private ObjectId picture;
    private Boolean isCards = false;
    private Boolean isNomer = false;

    public ObjectId getId(){
        return id;
    }
    public void setId(ObjectId id){
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public ObjectId getPicture() {
        return picture;
    }

    public void setPicture(ObjectId picture) {
        this.picture = picture;
    }

    public Boolean getIsCards() {
        return isCards;
    }

    public void setIsCards(Boolean isCards) {
        this.isCards = isCards;
    }

    public Boolean getIsNomer() {
        return isNomer;
    }

    public void setIsNomer(Boolean isNomer) {
        this.isNomer = isNomer;
    }
}
