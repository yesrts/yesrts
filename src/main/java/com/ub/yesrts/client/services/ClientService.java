package com.ub.yesrts.client.services;

import com.ub.yesrts.client.models.ClientDoc;
import  com.ub.yesrts.client.views.all.SearchClientAdminRequest;
import  com.ub.yesrts.client.views.all.SearchClientAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.List;

@Component
public class ClientService {

    @Autowired private MongoTemplate mongoTemplate;

    public ClientDoc save(ClientDoc clientDoc){
            mongoTemplate.save(clientDoc);
            return clientDoc;
    }

    public ClientDoc findById(ObjectId id){
            return mongoTemplate.findById(id, ClientDoc.class);
    }

    public void remove(ObjectId id){
            mongoTemplate.remove(findById(id));
    }

    public SearchClientAdminResponse findAll(SearchClientAdminRequest searchClientAdminRequest) {
            Sort sort = new Sort(Sort.Direction.DESC, "id");
            Pageable pageable = new PageRequest(
                    searchClientAdminRequest.getCurrentPage(),
                    searchClientAdminRequest.getPageSize(),
                    sort);

            Criteria criteria = new Criteria();
            //Criteria.where("title").regex(searchCompanyAdminRequest.getQuery(), "i");

            Query query = new Query(criteria);
            Long count = mongoTemplate.count(query, ClientDoc.class);
            query = query.with(pageable);

            List<ClientDoc> result = mongoTemplate.find(query, ClientDoc.class);
            SearchClientAdminResponse searchClientAdminResponse = new SearchClientAdminResponse(
                    searchClientAdminRequest.getCurrentPage(),
                    searchClientAdminRequest.getPageSize(),
                    result);
            searchClientAdminResponse.setAll(count);
            searchClientAdminResponse.setQuery(searchClientAdminRequest.getQuery());
            return searchClientAdminResponse;
    }

    public List<ClientDoc> findAll() {
        return mongoTemplate.find(new Query().with(new Sort(Sort.Direction.DESC, "position")), ClientDoc.class);
    }

    public List<ClientDoc> findForIndexPage() {
        return mongoTemplate.find(new Query(Criteria.where("isCards").is(true)).with(new Sort(Sort.Direction.DESC, "position")).limit(12), ClientDoc.class);
    }

    public List<ClientDoc> findForGarderPage() {
        return mongoTemplate.find(new Query(Criteria.where("isNomer").is(true)).with(new Sort(Sort.Direction.DESC, "position")).limit(12), ClientDoc.class);
    }
}
