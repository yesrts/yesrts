package com.ub.yesrts.feedback.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.services.PictureService;
import com.ub.yesrts.feedback.models.FeedbackDoc;
import com.ub.yesrts.feedback.routes.FeedbackAdminRoutes;
import com.ub.yesrts.feedback.services.FeedbackService;
import com.ub.yesrts.feedback.views.all.SearchFeedbackAdminRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class FeedbackAdminController {
    @Autowired private FeedbackService feedbackService;
    @Autowired private PictureService pictureService;


    @RequestMapping(value = FeedbackAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        FeedbackDoc feedbackDoc = new FeedbackDoc();
        feedbackDoc.setId(new ObjectId());
        feedbackDoc.setPicture(new ObjectId());
        model.addAttribute("feedbackDoc", feedbackDoc);
        return "com.ub.yesrts.admin.feedback.add" ;
    }

    @RequestMapping(value = FeedbackAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute FeedbackDoc feedbackDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        ObjectId id = pictureService.saveWithDelete(pic,null);
        feedbackDoc.setPicture(id);
        feedbackService.save(feedbackDoc);
        redirectAttributes.addAttribute("id",feedbackDoc.getId());
        return RouteUtils.redirectTo(FeedbackAdminRoutes.EDIT);
    }

    @RequestMapping(value = FeedbackAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        FeedbackDoc feedbackDoc = feedbackService.findById(id);
        if (feedbackDoc.getPicture() == null) feedbackDoc.setPicture(new ObjectId());
        model.addAttribute("feedbackDoc", feedbackDoc);
        return "com.ub.yesrts.admin.feedback.edit" ;
    }

    @RequestMapping(value = FeedbackAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute FeedbackDoc feedbackDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        FeedbackDoc old = feedbackService.findById(feedbackDoc.getId());
        ObjectId id = pictureService.saveWithDelete(pic, old.getPicture());
        if(id!=null){
            feedbackDoc.setPicture(id);
        }
        feedbackService.save(feedbackDoc);
        redirectAttributes.addAttribute("id",feedbackDoc.getId());
        return RouteUtils.redirectTo(FeedbackAdminRoutes.EDIT);
    }

     @RequestMapping(value = FeedbackAdminRoutes.ALL, method = RequestMethod.GET)
     public String all( @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                           @RequestParam(required = false, defaultValue = "") String query,
                           Model model) {
            SearchFeedbackAdminRequest searchFeedbackAdminRequest = new SearchFeedbackAdminRequest(currentPage);
            searchFeedbackAdminRequest.setQuery(query);
            model.addAttribute("searchFeedbackAdminResponse", feedbackService.findAll(searchFeedbackAdminRequest));
            return "com.ub.yesrts.admin.feedback.all";
     }

    @RequestMapping(value = FeedbackAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
          return "com.ub.yesrts.admin.feedback.delete" ;
    }

    @RequestMapping(value = FeedbackAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        feedbackService.remove(id);
        return RouteUtils.redirectTo(FeedbackAdminRoutes.ALL);
    }
}
