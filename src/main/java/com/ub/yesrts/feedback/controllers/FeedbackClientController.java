package com.ub.yesrts.feedback.controllers;

import com.ub.core.seo.seoTags.models.SeoTags;
import com.ub.yesrts.feedback.models.FeedbackDoc;
import com.ub.yesrts.feedback.routes.FeedbackRoutes;
import com.ub.yesrts.feedback.services.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class FeedbackClientController {
    @Autowired private FeedbackService feedbackService;

    @RequestMapping(value = FeedbackRoutes.ALL, method = RequestMethod.GET)
    public String all(Model model) {
        List<FeedbackDoc> feedbackDocList = feedbackService.findAll();
        SeoTags seoTags = new SeoTags();
        seoTags.setTitle("Шекспир | Все отзывы");
        seoTags.setMetaTitle("Шекспир | Все отзывы");
        seoTags.setMetaDescription("");
        seoTags.setH1("Все отзывы");
        model.addAttribute("feedbacks", feedbackDocList);
        model.addAttribute("seoTags", seoTags);
        return "com.ub.yesrts.client.feedback.all";
    }
}
