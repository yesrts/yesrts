package com.ub.yesrts.feedback.menu;

import com.ub.yesrts.feedback.routes.FeedbackAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class FeedbackAddMenu extends CoreMenu {
    public FeedbackAddMenu() {
        this.name ="Добавить";
        this.parent = new FeedbackMenu();
        this.url = FeedbackAdminRoutes.ADD;
    }
}
