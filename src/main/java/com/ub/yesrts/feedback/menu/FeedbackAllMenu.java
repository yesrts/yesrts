package com.ub.yesrts.feedback.menu;

import com.ub.yesrts.feedback.routes.FeedbackAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class FeedbackAllMenu extends CoreMenu {
    public FeedbackAllMenu() {
        this.name ="Все";
        this.parent = new FeedbackMenu();
        this.url = FeedbackAdminRoutes.ALL;
    }
}
