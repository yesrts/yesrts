package com.ub.yesrts.feedback.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class FeedbackMenu extends CoreMenu{
    public FeedbackMenu() {
        this.name = "Отзывы";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
