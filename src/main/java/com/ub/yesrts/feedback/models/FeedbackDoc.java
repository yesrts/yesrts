package com.ub.yesrts.feedback.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
public class FeedbackDoc {
    @Id
    private ObjectId id;
    private String name;
    private String company;
    private String position;
    private String text;
    private Boolean available;
    private ObjectId picture;
    private Boolean isCards = false;
    private Boolean isNomer = false;

    public ObjectId getId(){
        return id;
    }
    public void setId(ObjectId id){
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ObjectId getPicture() {
        return picture;
    }

    public void setPicture(ObjectId picture) {
        this.picture = picture;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getIsCards() {
        return isCards;
    }

    public void setIsCards(Boolean isCards) {
        this.isCards = isCards;
    }

    public Boolean getIsNomer() {
        return isNomer;
    }

    public void setIsNomer(Boolean isNomer) {
        this.isNomer = isNomer;
    }
}
