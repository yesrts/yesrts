package com.ub.yesrts.feedback.routes;

import com.ub.core.base.routes.BaseRoutes;

public class FeedbackAdminRoutes {
    private static final String ROOT = BaseRoutes.ADMIN + "/feedback";

    public static final String ADD = ROOT + "/add";
    public static final String EDIT = ROOT + "/edit";
    public static final String ALL = ROOT + "/all";
    public static final String DELETE = ROOT + "/delete";

}
