package com.ub.yesrts.feedback.services;

import com.ub.yesrts.feedback.models.FeedbackDoc;
import  com.ub.yesrts.feedback.views.all.SearchFeedbackAdminRequest;
import  com.ub.yesrts.feedback.views.all.SearchFeedbackAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.List;

@Component
public class FeedbackService {

    @Autowired private MongoTemplate mongoTemplate;

    public FeedbackDoc save(FeedbackDoc feedbackDoc){
            mongoTemplate.save(feedbackDoc);
            return feedbackDoc;
    }

    public FeedbackDoc findById(ObjectId id){
            return mongoTemplate.findById(id, FeedbackDoc.class);
    }

    public void remove(ObjectId id){
            mongoTemplate.remove(findById(id));
    }

    public SearchFeedbackAdminResponse findAll(SearchFeedbackAdminRequest searchFeedbackAdminRequest) {
            Sort sort = new Sort(Sort.Direction.DESC, "id");
            Pageable pageable = new PageRequest(
                    searchFeedbackAdminRequest.getCurrentPage(),
                    searchFeedbackAdminRequest.getPageSize(),
                    sort);

            Criteria criteria = new Criteria();
            //Criteria.where("title").regex(searchCompanyAdminRequest.getQuery(), "i");

            Query query = new Query(criteria);
            Long count = mongoTemplate.count(query, FeedbackDoc.class);
            query = query.with(pageable);

            List<FeedbackDoc> result = mongoTemplate.find(query, FeedbackDoc.class);
            SearchFeedbackAdminResponse searchFeedbackAdminResponse = new SearchFeedbackAdminResponse(
                    searchFeedbackAdminRequest.getCurrentPage(),
                    searchFeedbackAdminRequest.getPageSize(),
                    result);
            searchFeedbackAdminResponse.setAll(count);
            searchFeedbackAdminResponse.setQuery(searchFeedbackAdminRequest.getQuery());
            return searchFeedbackAdminResponse;
    }

    public List<FeedbackDoc> findAll() {
        return mongoTemplate.find(new Query(new Criteria("available").is(true)), FeedbackDoc.class);
    }

    public List<FeedbackDoc> findLastTwoFeedbacksForIndex() {
        return mongoTemplate.find(new Query(new Criteria("available").is(true).and("isCards").is(true)).limit(2), FeedbackDoc.class);
    }

    public List<FeedbackDoc> findLastTwoFeedbacksForNomer() {
        return mongoTemplate.find(new Query(new Criteria("available").is(true).and("isNomer").is(true)).limit(2), FeedbackDoc.class);
    }
}
