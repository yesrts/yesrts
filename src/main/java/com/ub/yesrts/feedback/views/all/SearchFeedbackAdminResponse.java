package com.ub.yesrts.feedback.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.yesrts.feedback.models.FeedbackDoc;

import java.util.List;

public class SearchFeedbackAdminResponse extends SearchResponse {
    private List<FeedbackDoc> result;


    public SearchFeedbackAdminResponse() {
    }

    public SearchFeedbackAdminResponse(Integer currentPage, Integer pageSize, List<FeedbackDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<FeedbackDoc> getResult() {
        return result;
    }

    public void setResult(List<FeedbackDoc> result) {
        this.result = result;
    }
}
