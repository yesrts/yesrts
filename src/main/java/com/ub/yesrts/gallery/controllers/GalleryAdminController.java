package com.ub.yesrts.gallery.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.services.PictureService;
import com.ub.yesrts.gallery.models.GalleryDoc;
import com.ub.yesrts.gallery.routes.GalleryAdminRoutes;
import com.ub.yesrts.gallery.services.GalleryService;
import com.ub.yesrts.gallery.views.all.SearchGalleryAdminRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class GalleryAdminController {
    @Autowired private GalleryService galleryService;
    @Autowired private PictureService pictureService;

    @RequestMapping(value = GalleryAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        GalleryDoc galleryDoc = new GalleryDoc();
        galleryDoc.setId(new ObjectId());
        galleryDoc.setPicture(new ObjectId());
        model.addAttribute("galleryDoc", galleryDoc);
        return "com.ub.yesrts.admin.gallery.add" ;
    }

    @RequestMapping(value = GalleryAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute GalleryDoc galleryDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        ObjectId id = pictureService.saveWithDelete(pic,null);
        galleryDoc.setPicture(id);
        galleryService.save(galleryDoc);
        redirectAttributes.addAttribute("id",galleryDoc.getId());
        return RouteUtils.redirectTo(GalleryAdminRoutes.EDIT);
    }

    @RequestMapping(value = GalleryAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        GalleryDoc galleryDoc = galleryService.findById(id);
        if (galleryDoc.getPicture() == null) galleryDoc.setPicture(new ObjectId());
        model.addAttribute("galleryDoc", galleryDoc);
        return "com.ub.yesrts.admin.gallery.edit" ;
    }

    @RequestMapping(value = GalleryAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute GalleryDoc galleryDoc,
                         @RequestParam(required = false) MultipartFile pic,
                         RedirectAttributes redirectAttributes) {
        GalleryDoc old = galleryService.findById(galleryDoc.getId());
        ObjectId id = pictureService.saveWithDelete(pic, old.getPicture());
        if(id!=null){
            galleryDoc.setPicture(id);
        }
        galleryService.save(galleryDoc);
        redirectAttributes.addAttribute("id",galleryDoc.getId());
        return RouteUtils.redirectTo(GalleryAdminRoutes.EDIT);
    }

     @RequestMapping(value = GalleryAdminRoutes.ALL, method = RequestMethod.GET)
     public String all( @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                           @RequestParam(required = false, defaultValue = "") String query,
                           Model model) {
            SearchGalleryAdminRequest searchGalleryAdminRequest = new SearchGalleryAdminRequest(currentPage);
            searchGalleryAdminRequest.setQuery(query);
            model.addAttribute("searchGalleryAdminResponse", galleryService.findAll(searchGalleryAdminRequest));
            return "com.ub.yesrts.admin.gallery.all";
     }

    @RequestMapping(value = GalleryAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
          return "com.ub.yesrts.admin.gallery.delete" ;
    }

    @RequestMapping(value = GalleryAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        galleryService.remove(id);
        return RouteUtils.redirectTo(GalleryAdminRoutes.ALL);
    }
}
