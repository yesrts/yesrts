package com.ub.yesrts.gallery.menu;

import com.ub.yesrts.gallery.routes.GalleryAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class GalleryAddMenu extends CoreMenu {
    public GalleryAddMenu() {
        this.name ="Добавить";
        this.parent = new GalleryMenu();
        this.url = GalleryAdminRoutes.ADD;
    }
}
