package com.ub.yesrts.gallery.menu;

import com.ub.yesrts.gallery.routes.GalleryAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class GalleryAllMenu extends CoreMenu {
    public GalleryAllMenu() {
        this.name ="Все";
        this.parent = new GalleryMenu();
        this.url = GalleryAdminRoutes.ALL;
    }
}
