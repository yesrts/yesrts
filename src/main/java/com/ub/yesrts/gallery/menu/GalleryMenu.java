package com.ub.yesrts.gallery.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class GalleryMenu extends CoreMenu{
    public GalleryMenu() {
        this.name = "Галерея";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
