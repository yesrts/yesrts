package com.ub.yesrts.gallery.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
public class GalleryDoc {
    @Id
    private ObjectId id;
    private Integer position = 1;
    private ObjectId picture;
    private Boolean isCards = false;
    private Boolean isNomer = false;
    private Boolean isMenu = false;

    public ObjectId getId(){
        return id;
    }
    public void setId(ObjectId id){
        this.id = id;
    }
    
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public ObjectId getPicture() {
        return picture;
    }

    public void setPicture(ObjectId picture) {
        this.picture = picture;
    }

    public Boolean getIsCards() {
        return isCards;
    }

    public void setIsCards(Boolean isCards) {
        this.isCards = isCards;
    }

    public Boolean getIsNomer() {
        return isNomer;
    }

    public void setIsNomer(Boolean isNomer) {
        this.isNomer = isNomer;
    }

    public Boolean getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Boolean isMenu) {
        this.isMenu = isMenu;
    }
}
