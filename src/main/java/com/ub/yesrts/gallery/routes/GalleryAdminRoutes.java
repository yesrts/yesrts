package com.ub.yesrts.gallery.routes;

import com.ub.core.base.routes.BaseRoutes;

public class GalleryAdminRoutes {
    private static final String ROOT = BaseRoutes.ADMIN + "/gallery";

    public static final String ADD = ROOT + "/add";
    public static final String EDIT = ROOT + "/edit";
    public static final String ALL = ROOT + "/all";
    public static final String DELETE = ROOT + "/delete";

}
