package com.ub.yesrts.gallery.services;

import com.ub.yesrts.gallery.models.GalleryDoc;
import  com.ub.yesrts.gallery.views.all.SearchGalleryAdminRequest;
import  com.ub.yesrts.gallery.views.all.SearchGalleryAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.List;

@Component
public class GalleryService {

    @Autowired private MongoTemplate mongoTemplate;

    public GalleryDoc save(GalleryDoc galleryDoc){
            mongoTemplate.save(galleryDoc);
            return galleryDoc;
    }

    public GalleryDoc findById(ObjectId id){
            return mongoTemplate.findById(id, GalleryDoc.class);
    }

    public void remove(ObjectId id){
            mongoTemplate.remove(findById(id));
    }

    public SearchGalleryAdminResponse findAll(SearchGalleryAdminRequest searchGalleryAdminRequest) {
            Sort sort = new Sort(Sort.Direction.DESC, "id");
            Pageable pageable = new PageRequest(
                    searchGalleryAdminRequest.getCurrentPage(),
                    searchGalleryAdminRequest.getPageSize(),
                    sort);

            Criteria criteria = new Criteria();
            //Criteria.where("title").regex(searchCompanyAdminRequest.getQuery(), "i");

            Query query = new Query(criteria);
            Long count = mongoTemplate.count(query, GalleryDoc.class);
            query = query.with(pageable);

            List<GalleryDoc> result = mongoTemplate.find(query, GalleryDoc.class);
            SearchGalleryAdminResponse searchGalleryAdminResponse = new SearchGalleryAdminResponse(
                    searchGalleryAdminRequest.getCurrentPage(),
                    searchGalleryAdminRequest.getPageSize(),
                    result);
            searchGalleryAdminResponse.setAll(count);
            searchGalleryAdminResponse.setQuery(searchGalleryAdminRequest.getQuery());
            return searchGalleryAdminResponse;
    }

    public List<GalleryDoc> findAll() {
        return mongoTemplate.find(new Query().with(new Sort(Sort.Direction.DESC, "position")),GalleryDoc.class);
    }

    public List<GalleryDoc> findAllCards() {
        return mongoTemplate.find(new Query(Criteria.where("isCards").is(true)).with(new Sort(Sort.Direction.DESC, "position")), GalleryDoc.class);
    }

    public List<GalleryDoc> findAllNomer() {
        return mongoTemplate.find(new Query(Criteria.where("isNomer").is(true)).with(new Sort(Sort.Direction.DESC, "position")), GalleryDoc.class);
    }
}
