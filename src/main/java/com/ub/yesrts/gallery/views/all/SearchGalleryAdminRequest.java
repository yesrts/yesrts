package com.ub.yesrts.gallery.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchGalleryAdminRequest extends SearchRequest{
    public SearchGalleryAdminRequest() {
    }

    public SearchGalleryAdminRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchGalleryAdminRequest(Integer currentPage, Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
