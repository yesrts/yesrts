package com.ub.yesrts.gallery.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.yesrts.gallery.models.GalleryDoc;

import java.util.List;

public class SearchGalleryAdminResponse extends SearchResponse {
    private List<GalleryDoc> result;


    public SearchGalleryAdminResponse() {
    }

    public SearchGalleryAdminResponse(Integer currentPage, Integer pageSize, List<GalleryDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<GalleryDoc> getResult() {
        return result;
    }

    public void setResult(List<GalleryDoc> result) {
        this.result = result;
    }
}
