package com.ub.yesrts.index.controllers;

import com.ub.core.seo.seoTags.models.SeoTags;
import com.ub.yesrts.client.services.ClientService;
import com.ub.yesrts.feedback.services.FeedbackService;
import com.ub.yesrts.gallery.services.GalleryService;
import com.ub.yesrts.index.routes.IndexRoutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class IndexController {
    @Autowired private GalleryService galleryService;
    @Autowired private FeedbackService feedbackService;
    @Autowired private ClientService clientService;
    @Autowired private MailSender mailSender;

    @RequestMapping(value = IndexRoutes.ROOT, method = RequestMethod.GET)
    public String index(Model model) {
        SeoTags seoTags = new SeoTags();
        seoTags.setTitle("Пластиковые карты, изготовление и дизайн");
        seoTags.setH1("Пластиковые карты, изготовление и дизайн");
        seoTags.setMetaTitle("Пластиковые карты, изготовление и дизайн");
        seoTags.setMetaDescription("Изготовление пластиковых карт в Волгограде по приемлемым ценам.");

        model.addAttribute("seoTags", seoTags);
        model.addAttribute("galleryDocs", galleryService.findAllCards());
        model.addAttribute("feedbackDocs", feedbackService.findLastTwoFeedbacksForIndex());
        model.addAttribute("clientDocs", clientService.findForIndexPage());
        return "com.ub.yesrts.client.index";
    }

    @RequestMapping(value = IndexRoutes.GARDEROBNYE_NOMERKI, method = RequestMethod.GET)
    public String nomer(Model model) {
        SeoTags seoTags = new SeoTags();
        seoTags.setTitle("Гардеробные номерки");
        seoTags.setH1("Гардеробные номерки");
        seoTags.setMetaTitle("Гардеробные номерки");
        seoTags.setMetaDescription("Производство гардеробных номерков в Волгограде");

        model.addAttribute("seoTags", seoTags);
        model.addAttribute("galleryDocs", galleryService.findAllNomer());
        model.addAttribute("feedbackDocs", feedbackService.findLastTwoFeedbacksForNomer());
        model.addAttribute("clientDocs", clientService.findForGarderPage());
        return "com.ub.yesrts.client.nomer";
    }

    @RequestMapping(value = "/card-order", method = RequestMethod.POST)
    public @ResponseBody String cardOrder(@RequestBody CardOrder order) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("yesrts@yandex.ru");
        message.setTo(new String[]{"989399@bk.ru", "rtstudia@gmail.com", "artem.suyazov@gmail.com"});
        message.setSubject("Заказ c yesrts.ru");
        message.setText("Поступил новый заказ<br>Имя: " + order.name + "<br>Email: " + order.email + "<br>Телефон: " + order.phone +
                "<br>Состав заказа:<br>" + order.text
        );
        mailSender.send(message);
        return "ok";
    }

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String p404(Model model) {

        return "com.ub.yesrts.client.404";
    }
    @RequestMapping(value = "/500", method = RequestMethod.GET)
    public String p500(Model model) {

        return "com.ub.yesrts.client.500";
    }

    public static class CardOrder {
        public String name;
        public String email;
        public String phone;
        public String text;
    }
}
