package com.ub.yesrts.seo;

import com.ub.core.seo.robotsCore.routes.RobotsCoreRoutes;
import com.ub.core.seo.robotsCore.services.RobotsCoreService;
import com.ub.core.seo.sitemap.routes.SitemapCoreRoutes;
import com.ub.core.seo.sitemap.service.SiteMapCoreService;
import com.ub.core.seo.sitemap.xml.UrlElement;
import com.ub.core.seo.sitemap.xml.UrlsetRootElement;
import com.ub.ubblog.blog.models.BlogDoc;
import com.ub.ubblog.blog.services.BlogService;
import com.ub.yesrts.index.routes.IndexRoutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SeoController {
    @Autowired private SiteMapCoreService siteMapCoreService;
    @Autowired private BlogService blogService;
    @Autowired private RobotsCoreService robotsCoreService;

    private final String host = "http://yesrts.ru";

    @ResponseBody
    @RequestMapping(value = RobotsCoreRoutes.ROOT, method = RequestMethod.GET)
    public String robots(){
        return robotsCoreService.getRobots().getRobots();
    }

    @ResponseBody
    @RequestMapping(value = SitemapCoreRoutes.ROOT_XML, method = RequestMethod.GET)
    public String sitemap() {
        UrlsetRootElement urlsetRootElement = new UrlsetRootElement();

        urlsetRootElement.getUrlElements().addAll(getStaticPages());
        urlsetRootElement.getUrlElements().addAll(getBlog());

        return siteMapCoreService.getSitemap(urlsetRootElement);
    }

    private List<UrlElement> getStaticPages() {
        List<UrlElement> urlElements = new ArrayList<UrlElement>();

        urlElements.add(new UrlElement(host + IndexRoutes.ROOT, "0.9"));

        urlElements.add(new UrlElement(host + SitemapCoreRoutes.ROOT_PAGE, "0.4"));

        return urlElements;
    }

    private List<UrlElement> getBlog() {
        List<UrlElement> urlElements = new ArrayList<UrlElement>();

        List<BlogDoc> blogDocs = blogService.findActiveAll();
        for (BlogDoc blogDoc : blogDocs) {
            try {
                urlElements.add(new UrlElement(host + "/"+blogDoc.getUrl(), "0.6"));
            } catch (Exception e) {
            }
        }

        return urlElements;
    }
}
