<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div"/>
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="name">Название</label>
        <form:input path="name" cssClass="form-control" id="name"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="url">Url</label>
        <form:input path="url" cssClass="form-control" id="url"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="position">Порядковый номер</label>
        <form:input path="position" cssClass="form-control" id="position"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <label for="available">Отображать в пласт. карт</label>
        <form:checkbox path="isCards" cssClass="form-control" id="available"/>
    </div>
    <div class="col-lg-6">
        <label for="available">Отображать в гард. номер.</label>
        <form:checkbox path="isNomer" cssClass="form-control" id="available"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="picture">Изображение</label>
        <form:hidden path="picture" cssClass="form-control"/>
        <input type="file" name="pic" id="picture"/>
    </div>
</div>