<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="position">Позиция</label>
        <form:input path="position" cssClass="form-control" id="position"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="picture">Изображение</label>
        <form:hidden path="picture" cssClass="form-control" />
        <input type="file" name="pic" id="picture"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label for="available">Отображать в пласт. карт</label>
        <form:checkbox path="isCards" cssClass="form-control" id="available"/>
    </div>
    <div class="col-lg-4">
        <label for="available">Отображать в гард. номер.</label>
        <form:checkbox path="isNomer" cssClass="form-control" id="available"/>
    </div>
    <div class="col-lg-4">
        <label for="available">Отображать в разледе меню.</label>
        <form:checkbox path="isMenu" cssClass="form-control" id="available"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <img src="/pics/${galleryDoc.picture}" style="max-width: 100%;"/>
    </div>
</div>