<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container page-content">
    <div class="row">
        <div class="col-md-12 padding-none">
            <div class="clearfix color3 h-title">
                <h1>
                    Наш блог
                </h1>
            </div>
        </div>
    </div>
    <c:forEach var="blogDoc" items="${blogDocList}">
        <div class="row articles">
            <div class="col-md-12">
                <article class="clearfix">
                    <div class="article-img">
                        <img src="/pics/${blogDoc.previewPic}" alt="${blogDoc.title}" title="${blogDoc.title}" style="width:100%">
                    </div>
                    <div class="article-text">
                        <h4><a href="/${blogDoc.url}">${blogDoc.title}</a></h4>
                        <p>${blogDoc.previewDescription}</p>
                        <p><a class="btn btn-default" href="/${blogDoc.url}">перейти к статье</a></p>
                    </div>
                </article>
            </div>
        </div>
    </c:forEach>
</div>