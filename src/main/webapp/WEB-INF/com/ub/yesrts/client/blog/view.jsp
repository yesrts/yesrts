<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container page-content">
    <div class="row">
        <div class="col-md-12 padding-none">
            <div class="clearfix color3 h-title">
                <h1>
                    ${blogDoc.title}
                </h1>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 blog-content">
            ${blogDoc.content}
            <div class="text-center">
                <script type="text/javascript">(function() {
                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                    }})();</script>
                <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="disqus_thread"></div>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
        </div>
    </div>
</div>
