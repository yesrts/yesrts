<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="clearfix color3 h-title" id="reviews">
                <h1>
                    Отзывы наших клиентов
                </h1>
            </div>
        </div>
    </div>

    <c:forEach items="${feedbacks}" var="feedbackDoc">
        <div class="row feedback">
            <div class="col-md-12 feedback-name">
                <h4>
                        ${feedbackDoc.company}
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 feedback-img">
                <img class="img-thumbnail " src="/pics/${feedbackDoc.picture}"/>
            </div>
            <div class="col-md-8">
                <div class="review-text">
                    <p>
                            ${feedbackDoc.text}
                    </p>
                    <small>${feedbackDoc.name},<br/>${feedbackDoc.position}</small>
                </div>
            </div>
        </div>
    </c:forEach>
    <div class="row" style="padding-bottom: 20px">
        <div class="col-md-12">
            <div class="centered-text">
                <a class="btn btn-default">оставить свой
                отзыв</a>
            </div>
        </div>
    </div>
</div>