<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="row">
    <div class="col-md-12 padding-none">
        <div id="calculator">
            <h3>
                Калькулятор цен для пластиковых карт
            </h3>

            <p class="calc-p-text text-center">
                С помощью нашего калькулятора вы легко рассчитаете стоимость вашего заказа.
            </p>
            <p class="attention">
                Бесплатная доставка по всей России!
            </p>
            <form onsubmit="return false;">
                <div class="amount">
                    <label for="amount">Введите количество:</label><input class="styler" id="amountOrder"
                                                                          oninput="calculator()" type="text"
                                                                          value="100"/><span> шт.</span>
                </div>
                <h4>
                    Опции:
                </h4>

                <div class="emboss">
                    <input id="emboss" type="checkbox"/>
                    <label class="padtop-5" for="emboss">Эмбоссирование</label>
                    <a class="tooltip" href="javascript:void(0);">
                        <i class="icon-info"></i>
                        <span class="classic">
                            Эмбоссирование - процесс механического выдавливания символов на пластиковых картах. пример ФИО на банковских картах или номер карты.
                        </span>
                    </a>
                    <span>&rarr;&nbsp;</span>
                    <input disabled="" id="typing" type="checkbox"/>
                    <label for="typing">Типирование</label>
                    <a class="tooltip" href="javascript:void(0);">
                        <i class="icon-info"></i>
                        <span class="classic">
                            Типирование - процесс окрашивания выдавленных символов в серебряный или золотой цвет, возможно окрашивание в другие цвета.
                        </span>
                    </a>

                    <div class="characters-div disabled hide">
                        <label for="characters"> &rarr; Количество знаков:</label>
                        <input class="styler disabled hide" disabled="disabled" id="characters" oninput="calculator()"
                            type="text" value="7"/>
                    </div>
                </div>
                <div class="magnet">
                    <input id="magnet_line" type="checkbox"/><label for="magnet_line">Магнитная полоса</label><a
                        class="tooltip" href="javascript:void(0);"><i class="icon-info"></i><span class="classic">Карта с магнитной полосой — тип карт, имеющих возможность хранения и изменения данных на магнитном носителе.</span></a><span>&rarr;&nbsp;</span><input
                        disabled="" id="protect_magnet_line" type="checkbox"/><label for="protect_magnet_line">Кодировка
                    магнитной полосы</label><a class="tooltip" href="javascript:void(0);"><i
                        class="icon-info"></i><span class="classic">Кодировка магнитной полосы - запись информации на магнитную полосу.</span></a>
                </div>
                <div class="lacquer">
                    <input id="lacquer" type="checkbox"/><label for="lacquer">Лакирование</label>
                    <span class="tooltip" href="javascript:void(0);">
                        <i class="icon-info"></i>
                        <span class="classic">Лакирование - нанесение лака, который образует выпуклый слой на готовой продукции.
                            <a href="http://yesrts.ru/fitness-club-sparta" target="_blank">Посмотреть фотографии</a>
                        </span>
                    </span>
                </div>
                <div class="chip">
                    <input id="chip" type="checkbox"/><label class="m-right0" for="chip">Чип</label>

                    <div class="chip-type hide">
                        <label> &rarr; выберите тип чипа:</label><input checked="checked" id="chip-emmarine"
                                                                        name="chip" type="radio"/><label
                            for="chip-emmarine">EM-marine</label><input id="chip-mifare" name="chip"
                                                                        type="radio"/><label
                            for="chip-mifare">Mifare </label>
                    </div>
                    <br/><input id="barcode" type="checkbox"/><label for="barcode">Штрих код</label>
                </div>
                <div class="color_plastic">
                    <h5>
                        Цвет пластика:
                    </h5>
                    <input checked="checked" id="plastic-color-white" name="plastic-color" type="radio"/><label
                        for="plastic-color-white">белый пластик</label><input id="plastic-color-gold"
                                                                              name="plastic-color"
                                                                              type="radio"/><label
                        for="plastic-color-gold">золотой пластик</label><input id="plastic-color-silver"
                                                                               name="plastic-color"
                                                                               type="radio"/><label
                        for="plastic-color-silver">серебрянный пластик</label><input id="transparent_color"
                                                                                     name="plastic-color"
                                                                                     type="radio"/><label
                        for="transparent_color">Прозрачный пластик</label>
                </div>
                <div class="thickness_plastic">
                    <h5>
                        Толщина пластика:
                    </h5>
                    <input checked="checked" id="middle-plastic" name="plastic-thickness" type="radio"/><label
                        for="middle-plastic">обычный пластик</label><a class="tooltip ml20mr10"
                                                                       href="javascript:void(0);"><i
                        class="icon-info"></i><span class="classic">Толщина обычной пластиковой карты 0.8 мм.</span></a><input
                        id="thick-plastic" name="plastic-thickness" type="radio"/><label for="thick-plastic">толстый
                    пластик</label><a class="tooltip ml20mr10" href="javascript:void(0);"><i
                        class="icon-info"></i><span
                        class="classic">Толщина толстой пластиковой карты - 1.1 мм.</span></a><input
                        id="thin-plastic" name="plastic-thickness" type="radio"/><label for="thin-plastic">тонкий
                    пластик</label><a class="tooltip ml20mr10" href="javascript:void(0);"><i
                        class="icon-info"></i><span
                        class="classic">Толщина тонкой пластиковой карты - 0.45 мм.</span></a>
                </div>
                <div class="single_options">
                    <p>
                        <input id="design" type="checkbox"/><label for="design">Разработка дизайна макета</label>
                    </p>

                    <p>
                        <input id="24h-deal" type="checkbox"/><label for="24h-deal">Срочное изготовление (в течение
                        24-х часов)</label><a class="tooltip" href="javascript:void(0);"><i
                            class="icon-info"></i><span class="classic">Заказы на срочное изготовление принимаются по согласованию с менеджером, стоимость + 20% к стоимости всего заказа, но не менее 1000 р.</span></a>
                    </p>
                </div>
                <div class="your-price">
                    <h4>
                        Стоимость вашего заказа:
                    </h4>

                    <p class="mtop10">
                            <span class="priceOne">Цена за карту:&nbsp;<output id="priceOne">18.00
                            </output> руб.</span><span class="priceOrder">Цена за тираж:&nbsp;<output id="priceOrder">1
                        800
                    </output> руб.</span>
                        <a class="btn btn-default btn-order" id="sendCardOrder">Заказать</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="card-order" style="display: none">
    <div id="card-order-description"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="card-order-name">Ваше имя</label>
                <input type="text" class="form-control" id="card-order-name" placeholder="Ваше имя">
            </div>
            <div class="form-group">
                <label for="card-order-email">Email</label>
                <input type="email" class="form-control" id="card-order-email" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="card-order-phone">Телефон</label>
                <input type="text" class="form-control" id="card-order-phone" placeholder="Телефон">
            </div>
            <button id="card-order-send" type="button" class="btn btn-default">Отправить</button>
        </div>
    </div>
</div>
