<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12 padding-none">
        <div class="carousel slide h-title" data-ride="carousel" id="clients">
            <h3>
                Наши клиенты
            </h3>
        </div>
    </div>
</div>
<div class="row clients">
    <c:forEach items="${clientDocs}" var="clientDoc">
        <div class="col-md-4">
            <img src="/pics/${clientDoc.picture}" title="Наш клиент - ${clientDoc.name}" alt=""/>
        </div>
    </c:forEach>
</div>