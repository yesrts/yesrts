<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12 padding-none">
        <div class="clearfix color3 h-title contacts">
            <h3>
                Контактная информация
            </h3>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12" style="padding: 30px">
        <p>
            <b>Адрес:</b> 400131, г. Волгоград, пр-кт Ленина, 15 (вход со двора)
        </p>

        <p>
            <b>Телефоны:</b> +7 (905) 339-27-65
        </p>

        <p>
            <b>Email: </b><a href="mailto:rtstudia@gmail.com">rtstudia@gmail.com</a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-12 padding-none">
        <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=6sfa83XhpW8bH4YPbGpJ0sL6DuVNmbtL&width=100%&height=420&lang=ru_RU&sourceType=constructor"></script>
    </div>
</div>