<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.yesrts.feedback.routes.FeedbackRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12 padding-none">
        <div class="clearfix color3 h-title" id="reviews">
            <h3>
                Отзывы наших клиентов
            </h3>
        </div>
    </div>
</div>

<c:forEach items="${feedbackDocs}" var="feedbackDoc">
    <div class="row feedback">
        <div class="col-md-3 text-center">
            <h4>
                    ${feedbackDoc.company}
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 feedback-img">
            <img class="img-thumbnail " src="/pics/${feedbackDoc.picture}" alt="" title="Отзыв о наших картах. ${feedbackDoc.name}"/>
        </div>
        <div class="col-md-8">
            <div class="review-text">
                <p>
                        ${feedbackDoc.text}
                </p>
                <small>${feedbackDoc.name},<br/>${feedbackDoc.position}</small>
            </div>
        </div>
    </div>
</c:forEach>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-12">
        <div class="centered-text">
            <a href="<%=FeedbackRoutes.ALL%>" class="btn btn-default">посмотреть все отзывы</a>
            <a class="btn btn-default">оставить свой отзыв</a>
        </div>
    </div>
</div>