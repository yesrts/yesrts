<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12 padding-none">
        <div class="carousel slide" data-ride="carousel" id="slider">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <c:forEach var="galleryItem" items="${galleryDocs}" varStatus="stat">
                    <li
                            <c:if test="${stat.first}">class="active"</c:if> data-slide-to="${stat.count}"
                            data-target="#slider"></li>
                </c:forEach>
            </ol>
            <!--Wrapper for slides-->
            <div class="carousel-inner">
                <c:forEach var="galleryItem" items="${galleryDocs}" varStatus="stat">
                    <div class="item <c:if test="${stat.first}">active</c:if>">
                        <img src="/pics/${galleryItem.getPicture()}" alt="Карусель с пластиковыми картами" title="Карусель с пластиковыми картами"/>
                    </div>
                </c:forEach>
            </div>
            <!--Controls-->
            <a class="left carousel-control" data-slide="prev" href="#slider"></a>
            <a class="right carousel-control" data-slide="next" href="#slider"></a>
        </div>
    </div>
</div>