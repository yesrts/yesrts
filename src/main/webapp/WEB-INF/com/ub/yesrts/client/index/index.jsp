<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container page-content">
    <jsp:include page="components/slider.jsp"/>
    <div class="row">
        <div class="col-md-12 padding-none">
            <div class="carousel slide h-title" data-ride="carousel" id="clients">
                <h1>
                    Пластиковые карты
                </h1>
            </div>
        </div>
    </div>
    <jsp:include page="components/calc.jsp"/>

    <jsp:include page="components/clients.jsp"/>

    <jsp:include page="components/revews.jsp"/>
    <div class="row">
        <div class="col-md-12 padding-none">
            <div class="clearfix color3 h-title" id="reviews">
                <h3>
                    Рекламно-творческая студия “Шекспир”
                </h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                <br/>
                Рекламно-творческая студия “Шекспир” - это команда лучших профессионалов, занимающихся <b>производством
                пластиковых карт в Волгограде</b>. Мы работаем больше 5 лет в этом направлении, обслуживаем клиентов по
                всей
                России. Нами освоено множество технологий производства карт, дающих нам преимущество перед конкурентами.
                Большое разнообразие и широкий выбор продукции, выпускаемой на нашем производстве, помимо основной,
                также выделяют нас среди других производителей:
                <br/>
                <br/>
            <ul>
                <li>меню из пластика с красивой брошюровкой</li>
                <li>таблички на дверь</li>
                <li>информационные таблички на стол (н-р,
                    "зарезервировано")
                </li>
                <li>гардеробные номерки</li>
                <li>бейджи разных видов (на различных шнуровках, с магнитным
                    креплением)
                </li>
                <li>брелоки</li>
                <li>магниты на холодильник</li>
                <li>настенные календари</li>
                <li>жетоны для игр разных форматов и
                    форм
                </li>
                <li>удостоверения личности с чипами</li>
                <li>подстаканники</li>
                <li>настольные игры</li>
                <li>пригласительные</li>
                <li>визитки</li>
                <li>презентационные листы</li>
                <li>дисконтные карты с магнитной полосой</li>
                <li>скидочные карты со штрих-кодом</li>
            </ul>
            <br/>
            <br/>
            Вся эта продукция может быть сделана из разных видов пластика, имеющих разные цвета и толщину. Самые
            распространенные виды пластика: белый, супербелый, серебряный, перламутровый, золотой, тонкий, толстый,
            средний или прозрачный.
            <br/>
            <br/>
            Также существуют разные способы ламинации: матовая, глянцевая или рельефная. Мы постоянно развиваемся и
            ищем новые материалы и способы производства. Высокое качество нашей продукции, удобная форма работы,
            высокая производительность и профессионализм наших сотрудников были оценены сотнями наших клиентов.
            <br/>
            </p>
        </div>
    </div>
    <jsp:include page="components/contacts.jsp"/>
</div>