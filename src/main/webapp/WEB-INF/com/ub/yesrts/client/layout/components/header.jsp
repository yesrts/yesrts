<%@ page import="com.ub.yesrts.index.routes.IndexRoutes" %>
<%@ page import="com.ub.yesrts.blog.routes.BlogRoutes" %>
<div class="navbar navbar-default navbar-top navbar-custom">
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="logo">
                    <a href="/">
                        <img src="/static/yesrts/images/logo-846a99d8.png"/>
                    </a>

                    <div class="logo-title text-left">
                        <p>Шекспир</p>

                        <p>
                           Рекламно-творческая студия
                        </p>
                        <p><a href="tel:+79053392765">+7 (905) 339-27-65</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-8">
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="/">Пластиковые карты</a>
                    </li>
                    <li>
                        <a href="<%= IndexRoutes.GARDEROBNYE_NOMERKI%>">Гардеробные номерки</a>
                    </li>
                    <li>
                        <a href="<%=BlogRoutes.ALL%>">Блог</a>
                    </li>
                    <%--<li>--%>
                        <%--<a href="#">Контакты</a>--%>
                    <%--</li>--%>
                </ul>
            </div>
        </div>
    </div>
</div>