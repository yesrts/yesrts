$(document).ready(function() {
    var kn = $("#thick-plastic, #thin-plastic");
    var ksns = $("#thick-plastic-styler, #thin-plastic-styler");
    var nsnsl = $('#thin-plastic-styler, #thin-plastic-styler + label');
    var ksksl = $('#thick-plastic-styler, #thick-plastic-styler + label');

    $("#calculator div").attr("onclick", "calculator()");

// При выборе Эмбоссирования становится доступным Типирование и ввод знаков
    $("#emboss").on("click", function() {
        if ($('#emboss').prop('checked') === true) {
            $("#typing").removeAttr("disabled");
        }
        else {
            $("#typing").attr("disabled", "disabled");
        }
        $("#typing").removeAttr("checked");
        $('#typing-styler').removeClass('checked').toggleClass("disabled");
        $('.characters-div, #characters').toggleClass("disabled");

        if ( $("#characters").attr("disabled") ) {
            $("#characters").removeAttr("disabled");
        } else {
            $("#characters").attr("disabled", "disabled");
        }
    });
// При выборе Магнитная полоса становится доступным Кодировка магнитной полосы
    $("#magnet_line").on("click", function() {
        if ($('#magnet_line').prop('checked') === true) {
            $("#protect_magnet_line").removeAttr("disabled");
        }
        else {
            $("#protect_magnet_line").attr("disabled", "disabled");
        }
        $("#protect_magnet_line").removeAttr("checked");
        $('#protect_magnet_line-styler').removeClass('checked') .toggleClass("disabled");
    });

// Отображение толщины пластика в зависимости от выбранного цвета
    $("#plastic-color-gold-styler, #plastic-color-silver-styler").on("click", function() {
        kn.removeAttr("checked");
        ksns.removeClass("checked");
        $("#middle-plastic").attr("checked", "checked");
        $("#middle-plastic-styler").addClass("checked");
        nsnsl.addClass("disabled");
        ksksl.removeClass("disabled");
    });

    $("#transparent_color-styler").on("click", function() {
        kn.removeAttr("checked");
        ksns.removeClass("checked");
        $("#middle-plastic").attr("checked", "checked");
        $("#middle-plastic-styler").addClass("checked");
        nsnsl.removeClass("disabled");
        ksksl.addClass("disabled");
    });

    $("#plastic-color-white-styler").on("click", function() {
        nsnsl.removeClass("disabled");
        ksksl.removeClass("disabled");
    });


    $('#sendCardOrder').click(function() {
        var amountOrderText = $("#amountOrder").val();
        var emboss = $('#emboss');
        var typing = $('#typing');
        var magnetLine = $("#magnet_line");
        var protectMagnetLine = $('#protect_magnet_line');
        var chip = $("#chip");
        var emmarine = $("#chip-emmarine");
        var mifare = $("#chip-mifare");
        var barcode = $("#barcode");
        var design = $("#design");
        var dayDeal = $("#24h-deal");
        var plcolorWhite = $("#plastic-color-white");
        var plcolorGold = $("#plastic-color-gold");
        var plcolorSilver = $("#plastic-color-silver");
        var plcolorTransparent = $("#transparent_color");
        var middlePl = $("#middle-plastic");
        var thickPl = $("#thick-plastic");
        var thinPl = $("#thin-plastic");

        var priceOnePrint = $("#priceOne").val();
        var priceOrderPrint = $("#priceOrder").val();

// эмбосирование и типирование
        if ( emboss.prop("checked") && !typing.prop("checked") ) {
            var a = "– Эмбоссирование.";
            var embossPrint = a + ".<br />";
        } else if ( emboss.prop("checked") && typing.prop("checked") ) {
            var a = "– Эмбоссирование и типирование.";
            var embossPrint = a + ".<br />";
        } else {
            var embossPrint = "";
        }

// магнитная полоса и кодировка
        if ( magnetLine.prop("checked") && !protectMagnetLine.prop("checked") ) {
            var c = "– Карта с магнитной полосой";
            var magnetPrint = c + ".<br />";
        } else if ( magnetLine.prop("checked") && protectMagnetLine.prop("checked") ) {
            var c = "– Карта с магнитной полосой и кодировкой";
            var magnetPrint = c + ".<br />";
        } else {
            var magnetPrint = "";
        }

        // лакирование
        var lacquerPrint = '';
        if ($('#lacquer').prop('checked')) {
            lacquerPrint += '- Лакирование.<br/>';
        }

// чип
        if ( chip.prop("checked") && emmarine.prop("checked") ) {
            var d = "– Чип";
            var chipPrint = d + ".<br />";
        } else if ( chip.prop("checked") && mifare.prop("checked") ) {
            var d = "– Чип";
            var chipPrint = d + ".<br />";
        } else {
            var chipPrint = "";
        }

// штрих код
        if ( barcode.prop("checked") ) {
            var e = "– Штрих код";
            var barcodePrint = e + ".<br />";
        } else {
            var barcodePrint = "";
        }

// цвет пластика
        if ( plcolorWhite.prop("checked") ) {
            var h = "белый";
            var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
        } else if ( plcolorGold.prop("checked") ) {
            var h = "золотой";
            var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
        } else if ( plcolorSilver.prop("checked") ) {
            var h = "серебрянный";
            var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
        } else if ( plcolorTransparent.prop("checked") ) {
            var h = "прозрачный";
            var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
        } else {
            var plcolorPrint = "";
        }

// толщина пластика
        if ( middlePl.prop("checked") ) {
            var k = "обычный";
            var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";
        } else if ( thickPl.prop("checked") ) {
            var k = "толстый";
            var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";
        } else if ( thinPl.prop("checked") ) {
            var k = "тонкий";
            var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";
        } else {
            var thicknessPrint = "";
        }

// разработка дизайна макета
        if ( design.prop("checked") ) {
            var f = "– Разработка дизайна макета";
            var designPrint = f + ".<br />";
        } else {
            var designPrint = "";
        }

// срочное изготовление
        if ( dayDeal.prop("checked") ) {
            var g = "– Срочное изготовление (в течение 24-х часов)";
            var dayDealPrint = g;
        } else {
            var dayDealPrint = "";
        }

        /*       alert(embossPrint + magnetPrint + chipPrint + barcodePrint + plcolorPrint + thicknessPrint + designPrint + dayDealPrint); */
        //
        // $("textarea#amountOrderText").html(amountOrderText);
        // $("textarea#embossPrint").html(embossPrint);
        // $("textarea#magnetPrint").html(magnetPrint);
        // $("textarea#chipPrint").html(chipPrint);
        // $("textarea#barcodePrint").html(barcodePrint);
        // $("textarea#plcolorPrint").html(plcolorPrint);
        // $("textarea#thicknessPrint").html(thicknessPrint);
        // $("textarea#designPrint").html(designPrint);
        // $("textarea#dayDealPrint").html(dayDealPrint);
        //
        // $("textarea#priceOnePrint").html(priceOnePrint);
        // $("textarea#priceOrderPrint").html(priceOrderPrint);

        /*       $("textarea#text").html(amountOrderText + "&#x0A;" + embossPrint + magnetPrint + chipPrint + barcodePrint + plcolorPrint + "&#x0A;" + thicknessPrint + designPrint + dayDealPrint); */
        $("#card-order-description").html("<p class='edition'>Тираж: " + amountOrderText + " шт.</p> " +
            "<p class='option-title'>Выбранные опции:</p> <p>" + embossPrint + magnetPrint + lacquerPrint + chipPrint +
            barcodePrint + plcolorPrint + thicknessPrint + designPrint + dayDealPrint + "<p>" + "<p class='price'>Цена за карту: <span>" + priceOnePrint + "</span> руб.&nbsp;&nbsp;&nbsp;&nbsp;" + "Цена за тираж: <span>" + priceOrderPrint + "</span> руб.</p>");
        $('#card-order').dialog({
            width: 500
        });
    });

    $('#card-order-send').click(function() {
        var order = {
            name: $('#card-order-name').val(),
            email: $('#card-order-email').val(),
            phone: $('#card-order-phone').val(),
            text: $('#card-order-description').html()
        };

        if (order.email === '') {
            alert('Введите, пожалуйста, адрес электронной почты');
            return;
        }

        $.ajax({
            url: '/card-order',
            method: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(order),
            success: function(data) {
                $('#card-order').dialog('close');
                alert("Заказ успешно отправлен!");
            },
            error: function(e) {
                alert('Произошла ошибка, попробуйте повторить заказ позднее.');
            }
        })
    });
});


// Калькулятор

function calculator(){

    var priceOne = 0;
    var priceOrder = 0;
    var amountOrder = parseFloat(document.getElementById('amountOrder').value);

    if (amountOrder < 200) {
        priceOne = 18;
    }
    if (amountOrder < 500 && amountOrder >= 200) {
        priceOne = 16;
    }
    if (amountOrder < 1000 && amountOrder >= 500) {
        priceOne = 13;
    }
    if (amountOrder < 2000 && amountOrder >= 1000) {
        priceOne = 9;
    }
    if (amountOrder < 3000 && amountOrder >= 2000) {
        priceOne = 8;
    }
    if (amountOrder < 5000 && amountOrder >= 3000) {
        priceOne = 7.5;
    }
    if (amountOrder < 10000 && amountOrder >= 5000) {
        priceOne = 7;
    }
    if (amountOrder >= 10000) {
        priceOne = 6;
    }
    if (amountOrder <= 0) {
        amountOrder = 0;
    }


    if (document.getElementById('emboss').checked)
        if (parseFloat(document.getElementById('characters').value) <= 6) {
            priceOne = priceOne + (parseFloat(document.getElementById('characters').value) * 0.5);
        } else {
            priceOne = priceOne + 3;
        }

    if (document.getElementById('typing').checked) priceOne += 1;
    if (document.getElementById('magnet_line').checked)	priceOne += 2;
    if (document.getElementById('protect_magnet_line').checked) priceOne += 0.5;

    // лакирование
    if (document.getElementById('lacquer').checked) {
        if (amountOrder >= 300 && amountOrder < 5000) {
            priceOne += 10;
        }
        if (amountOrder >= 5000) {
            priceOne += 7;
        }
    }

//цвет пластика
    if (document.getElementById('plastic-color-gold').checked) priceOne += 2;
    if (document.getElementById('plastic-color-silver').checked) priceOne += 2;
    if (document.getElementById('transparent_color').checked) priceOne += 2;

//толщина пластика
    if (document.getElementById('thick-plastic').checked) priceOne += 4;
    if (document.getElementById('thin-plastic').checked) priceOne += 1;

    if (document.getElementById('barcode').checked) priceOne += 1;

    priceOrder = amountOrder * priceOne;

    if (document.getElementById('design').checked)	priceOrder += 700;
    if (document.getElementById('24h-deal').checked) {
        if (( (priceOrder/100)*20 ) >= 1000) {
            priceOrder = priceOrder + ( (priceOrder/100)*20 );
        } else {
            priceOrder = priceOrder + 1000;
        }
    }

    // лакирование
    if (document.getElementById('lacquer').checked) {
        if (amountOrder < 300) {
            priceOrder += 3000;
        }
    }

//разделеляем цифру стоимости заказа на разряды
    var x = String(priceOrder);

    x = x.replace(/.+?(?=\D|$)/, function(f) {
        return f.replace(/(\d)(?=(?:\d\d\d)+$)/g, "$1 ");
    });

//выводим цены
    document.getElementById('priceOne').innerHTML = priceOne.toFixed(2);
    document.getElementById('priceOrder').innerHTML = x;
}