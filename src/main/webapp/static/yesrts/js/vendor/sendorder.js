$(document).ready(function() {
// При выборе Эмбоссирования становится доступным Типирование и ввод знаков
  $("#sendOrder").on("click", function() {
  	var amountOrderText = $("#amountOrder").val();
    var emboss = $('#emboss-styler');
    var typing = $('#typing-styler');
    var magnetLine = $("#magnet_line-styler");
    var protectMagnetLine = $('#protect_magnet_line-styler');
    var chip = $("#chip-styler");
    var emmarine = $("#chip-emmarine-styler");
    var mifare = $("#chip-mifare-styler");
    var barcode = $("#barcode-styler");
    var design = $("#design-styler"); 
    var dayDeal = $("#24h-deal-styler"); 
    var plcolorWhite = $("#plastic-color-white-styler");
    var plcolorGold = $("#plastic-color-gold-styler");
    var plcolorSilver = $("#plastic-color-silver-styler");
    var plcolorTransparent = $("#transparent_color-styler");
    var middlePl = $("#middle-plastic-styler");
    var thickPl = $("#thick-plastic-styler");
    var thinPl = $("#thin-plastic-styler");
    
    var priceOnePrint = $("#priceOne").val();
    var priceOrderPrint = $("#priceOrder").val();    

// эмбосирование и типирование
      if ( emboss.hasClass("checked") && !typing.hasClass("checked") ) {
        var a = "– Эмбоссирование. Количество знаков: ";
        var b = $("#characters").val();
        var embossPrint = a + b + ".<br />";
      } else if ( emboss.hasClass("checked") && typing.hasClass("checked") ) {
        var a = "– Эмбоссирование и типирование. Количество знаков: ";
        var b = $("#characters").val();
        var embossPrint = a + b + ".<br />";
      } else {
        var embossPrint = "";
      }

// магнитная полоса и кодировка
      if ( magnetLine.hasClass("checked") && !protectMagnetLine.hasClass("checked") ) {
        var c = "– Карта с магнитной полосой";
        var magnetPrint = c + ".<br />";
      } else if ( magnetLine.hasClass("checked") && protectMagnetLine.hasClass("checked") ) {
        var c = "– Карта с магнитной полосой и кодировкой";
        var magnetPrint = c + ".<br />";
      } else {
        var magnetPrint = "";
      }

// чип
      if ( chip.hasClass("checked") && emmarine.hasClass("checked") ) {
        var d = "– Чип EM-marine";
        var chipPrint = d + ".<br />";
      } else if ( chip.hasClass("checked") && mifare.hasClass("checked") ) {
        var d = "– Чип Mirafe";
        var chipPrint = d + ".<br />";
      } else {
        var chipPrint = "";
      }

// штрих код
      if ( barcode.hasClass("checked") ) {
        var e = "– Штрих код";
        var barcodePrint = e + ".<br />";
      } else {
        var barcodePrint = "";
      }

// цвет пластика
      if ( plcolorWhite.hasClass("checked") ) {
        var h = "белый";
        var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
      } else if ( plcolorGold.hasClass("checked") ) {
        var h = "золотой";
        var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
      } else if ( plcolorSilver.hasClass("checked") ) {
        var h = "серебрянный";
        var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";
      } else if ( plcolorTransparent.hasClass("checked") ) {
        var h = "прозрачный";
        var plcolorPrint = "– Цвет пластика: " + h + " пластик" + ".<br />";                
      } else {
        var plcolorPrint = "";
      }

// толщина пластика
      if ( middlePl.hasClass("checked") ) {
        var k = "обычный";
        var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";
      } else if ( thickPl.hasClass("checked") ) {
        var k = "толстый";
        var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";
      } else if ( thinPl.hasClass("checked") ) {
        var k = "тонкий";
        var thicknessPrint = "– Толщина пластика: " + k + " пластик" + ".<br />";              
      } else {
        var thicknessPrint = "";
      }

// разработка дизайна макета
      if ( design.hasClass("checked") ) {
        var f = "– Разработка дизайна макета";
        var designPrint = f + ".<br />";
      } else {
        var designPrint = "";
      }

// срочное изготовление
      if ( dayDeal.hasClass("checked") ) {
        var g = "– Срочное изготовление (в течение 24-х часов)";
        var dayDealPrint = g;
      } else {
        var dayDealPrint = "";
      }

/*       alert(embossPrint + magnetPrint + chipPrint + barcodePrint + plcolorPrint + thicknessPrint + designPrint + dayDealPrint); */

      $("textarea#amountOrderText").html(amountOrderText);
      $("textarea#embossPrint").html(embossPrint);
      $("textarea#magnetPrint").html(magnetPrint);
      $("textarea#chipPrint").html(chipPrint); 
      $("textarea#barcodePrint").html(barcodePrint);
      $("textarea#plcolorPrint").html(plcolorPrint);
      $("textarea#thicknessPrint").html(thicknessPrint);                 
      $("textarea#designPrint").html(designPrint);
      $("textarea#dayDealPrint").html(dayDealPrint);
      
      $("textarea#priceOnePrint").html(priceOnePrint);
      $("textarea#priceOrderPrint").html(priceOrderPrint);            

/*       $("textarea#text").html(amountOrderText + "&#x0A;" + embossPrint + magnetPrint + chipPrint + barcodePrint + plcolorPrint + "&#x0A;" + thicknessPrint + designPrint + dayDealPrint); */
      $("div#text").html("<p class='edition'>Тираж: " + amountOrderText + " шт.</p> <p class='option-title'>Выбранные опции:</p> <p>" + embossPrint + magnetPrint + chipPrint + barcodePrint + plcolorPrint + thicknessPrint + designPrint + dayDealPrint + "<p>" + "<p class='price'>Цена за карту: <span>" + priceOnePrint + "</span> руб.&nbsp;&nbsp;&nbsp;&nbsp;" + "Цена за тираж: <span>" + priceOrderPrint + "</span> руб.</p>");      
  });  
});

$(document).ready(function() {
  $("#amountNomerok").on("keyup", function() {
    var priceOrderNomerokPrint = $("#priceOrderNomerok").val();    
      $("textarea#priceOrderNomerokPrint").html(priceOrderNomerokPrint);                  
  });  
});

// Калькулятор гардеробных номерков

function calcNomerok(){

var priceOrderNomerok = 0;
var amountNomerok = parseFloat(document.getElementById('amountNomerok').value);

priceOrderNomerok = amountNomerok * 25;


if (amountNomerok > 199) {
  document.getElementById("forDiscount").className += "discount";
} else {
	document.getElementById("forDiscount").className = document.getElementById("forDiscount").className.replace( /(?:^|\s)discount(?!\S)/ , '' );	
}


//разделеляем цифру стоимости заказа на разряды
var x = String(priceOrderNomerok);
      
x = x.replace(/.+?(?=\D|$)/, function(f) {
  return f.replace(/(\d)(?=(?:\d\d\d)+$)/g, "$1 ");
});

//выводим цены
document.getElementById('priceOrderNomerok').innerHTML = x;
}




